(function($) {
  var unionUnited = angular.module('unionUnited', 
    ['ngRoute']);

  unionUnited.config(['$routeProvider', 
    function($routeProvider) {
      $routeProvider.when('/home', {
        templateUrl: 'partials/home.html'
      }).when('/freedom', {
        templateUrl: 'partials/freedomschools.html'
      }).otherwise({redirectTo: '/home'});
    }
  ]);

  unionUnited.controller('mainController', ['$scope', function($scope) {
    $scope.$on('$routeChangeSuccess', function(event, current) {
      var path = current.$$route.originalPath.substring(1);
      $('.menu-link.active').removeClass('active')
      $('#' + path + '-link').addClass('active');
    });
  }]);

})(jQuery);